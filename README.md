<div style="font-size:36rem" align="center"><h1>👻</h1></div>
<br/>
This is a package designed to help optimize around Plutus's BuiltinData without losing all type safety.
<br/>

# Untyped Version

This is the default.

```haskell
type Spooky (a :: k) = BuiltinData
```

The phantom type is poly kinded for flexiblity and ergonomics. This allows for some safety as we can hold type information in the phantom type representing the semantics of our code.

## Note

Keep in mind the following works. So not that much safety is provided here:

```haskell
 λ. type Untyped a = String
 λ. untypedId = id :: Untyped Int -> Untyped Int
 λ. x = "wat" :: Untyped String
 λ. untypedId x
"wat"
```

# The Typed Version

Supplied along side the type alias is a newtype version that can be enabled with a cabal flag.

```bash
cabal build -f typed
```

will replace the type alias with a newtype

```haskell
newtype Spooky (a :: k) = Spooky BuiltinData
```

Which derives nearly all of `BuiltinData`s instances.

## Note

Keep in mind with the typed version the following works. So we have some real safety:

```haskell
 λ. newtype Typed a = Typed String deriving Show
 λ. typedId = id :: Typed Int -> Typed Int
 λ. y = Typed "wat" :: Typed String
 λ. typedId y

<interactive>:21:9: error:
    • Couldn't match type ‘[Char]’ with ‘Int’
      Expected type: Typed Int
        Actual type: Typed String
    • In the first argument of ‘typedId’, namely ‘y’
      In the expression: typedId y
      In an equation for ‘it’: it = typedId y
```

